export const TEXT_REQUEST = 'TEXT_REQUEST';
export const TEXT_SUCCESS = 'TEXT_SUCCESS';
export const TEXT_FAIL = 'TEXT_FAIL';

export default function handleButton() {
  return function(dispatch) {

    // dispatch click button:
    console.log('TEXT_REQUEST');

    dispatch({
      type: TEXT_REQUEST,
    });


    // emulate asynchronous request:
    setTimeout(() => {
      console.log('TEXT_SUCCESS');

      dispatch({
        type: TEXT_SUCCESS,
        payload: 'New text',
      });
    }, 1000);
  };
}
