import React from 'react';
import ReactDOM from 'react-dom';
import './styles.css';
import { Provider } from 'react-redux';
import store, { history } from './store/store';

import App from './components/App';
import {ConnectedRouter} from "connected-react-router";

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App/>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root'),
);
