import React from 'react';

export default class Button extends React.Component {
  onClickEnter = () => this.props.handleButton();

  render() {
    const { text, isFetching} = this.props.button;

    return (
      <div>
        <button onClick={this.onClickEnter}>{text}</button>

        {isFetching && <div>Loading...</div>}
      </div>

    );
  }
}
