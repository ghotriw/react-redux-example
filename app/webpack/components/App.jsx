import React from 'react';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router';
import { Link } from 'react-router-dom';
import handleButton from '../actions/ButtonActions';
import Button from './Button';

import PageIndex from './PageIndex';
import PageOther from './PageOther';

class App extends React.Component {
  render() {

    return (
      <React.Fragment>
        <Button {...this.props}/>

        <Link to='/'>page index</Link>
        <Link to='page-other'>page other</Link>

        <Switch>
          <Route exact path='/'>
            <PageIndex/>
          </Route>
          <Route path='/page-other'>
            <PageOther/>
          </Route>
        </Switch>

      </React.Fragment>
    );
  }
}

const mapStateToProps = store => {
  return {
    button: store.button,
    isFetching: store.isFetching,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    handleButton: () => dispatch(handleButton()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);
