import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { buttonReducer } from './button';

export default (history) => combineReducers({
  router: connectRouter(history),
  button: buttonReducer,
});
