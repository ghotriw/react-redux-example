import { TEXT_REQUEST, TEXT_SUCCESS, TEXT_FAIL } from '../actions/ButtonActions';

const initialState = window.initialStates.button;

export function buttonReducer(state = initialState, action) {
  switch (action.type) {

    case TEXT_REQUEST:
      return { ...state, isFetching: true, error: '' };
    case TEXT_SUCCESS:
      return { ...state, isFetching: false, text: action.payload };
    case TEXT_FAIL:
      return { ...state, isFetching: false, error: action.payload.message };

    default:
      return state;
  }
}
